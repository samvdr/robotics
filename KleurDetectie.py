import rospy
import numpy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from geometry_msgs.msg import Twist


class HerkenVorm:
    def __init__(self):
        self.bridge = CvBridge()

        # Connect image topic
        img_topic = "/camera/rgb/image_raw"
        self.image_sub = rospy.Subscriber(img_topic, Image, self.callback)
        self.cmd_vel_pub = rospy.Publisher('cmd_vel_mux/input/teleop', Twist, queue_size=1)
        self.twist = Twist()
        # Allow up to one second to connection
        rospy.sleep(1)

    def callback(self, data):

        cv_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='bgr8')
        hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

        # zoek geel
        lower_geel = numpy.array([10, 10, 10])
        upper_geel = numpy.array([255, 255, 250])
        mask_geel = cv2.inRange(hsv, lower_geel, upper_geel)

        #zoek rood
        lower_rood = numpy.array([0, 50, 50])
        upper_rood = numpy.array([10, 255, 255])
        mask_rood = cv2.inRange(hsv, lower_rood, upper_rood)

        mg = cv2.moments(mask_geel)
        mr = cv2.moments(mask_rood)

        # mg['m00'] > 0 and
        if mg['m00'] > 0 and mr['m00'] > 0:
            cgx = int(mg['m10'] / mg['m00'])
            cgy = int(mg['m01'] / mg['m00'])
            crx = int(mr['m10'] / mr['m00'])
            cry = int(mr['m01'] / mr['m00'])
            cv2.circle(cv_image, (cgx, cgy), 20, (0, 0, 255), -1)
            x = cv2.circle(cv_image, (cgx, cgy), 20, (0, 0, 255), -1)
            rospy.loginfo(x)
            cv2.circle(cv_image, (crx, cry), 20, (255, 0, 0), -1)
            y = cv2.circle(cv_image, (crx, cry), 20, (255, 0, 0), -1)
            # rospy.loginfo(y)
            self.twist.linear.x = 0.2
            self.twist.angular.z = 0
            self.cmd_vel_pub.publish(self.twist)

        cv2.imshow("window", cv_image)
        cv2.waitKey(3)


if __name__ == '__main__':
    rospy.init_node('vormHerkenning', anonymous=False)
    HerkenVorm = HerkenVorm()
    rospy.spin()
