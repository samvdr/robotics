#!/usr/bin/env python

import rospy
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import PointCloud2, PointField
from std_msgs.msg import Float64


class distanceNode:

    def __init__(self):
        # publisher die messages doorstuurt over topic /DISTANCE
        self.pub = rospy.Publisher('DISTANCE', Float64, queue_size=1)
        # subscriber die messages ontvangt over topic camera/depth/points en geeft die message mee aan callback_kinect
        rospy.Subscriber("/camera/depth/points", PointCloud2, self.callback_kinect)
        # zorgt ervoor dat het script niet wordt afgesloten
        rospy.spin()

    def callback_kinect(self, data):
        height = int(data.height / 2)
        middle_x = int(data.width / 2)
        self.read_depth(middle_x, height, data)

    def read_depth(self, width, height, data):
        if (height >= data.height) or (width >= data.width):
            return -1
        data_out = pc2.read_points(data, field_names="z", skip_nans=False, uvs=[[width, height]])
        float_data = float(next(data_out)[0])
        print(float_data)
        self.pub.publish(float_data)


if __name__ == '__main__':
    rospy.init_node('distance_node', anonymous=True)
    try:
        distanceNode()
    except rospy.ROSInterruptException:
        pass
