plaats deze mappen in de ros package bijvoorbeeld ~/catkin_ws/src/project_robotics
om te gebruiken
1. zorg dat "source ~/catkin_ws/devel/setup.bash" is toegevoegd aan .bashrc
2. roslaunch project_robotics projectworld.launch

Voor meer gates
1. voeg in gazebo kubus toe
2. wereld opslaan als en overschrijf project.world
3. sluit gazebo af en open project.world in gedit
4. zoek de juiste box en pas aan
	- size: 0.25 0.25 0.5
	-script name: gazebo/Blue voor links en gazebo/Red voor rechts

Je kan ook alles tussen de model-tag kopiëren maar zorg zeker dat je de coördinaten aanpast!
