#!/usr/bin/env python

import math
import rospy
from nav_msgs.msg import Odometry

landmarks = []
landmarks.append(("Dumpster", -0.44, -0.51))
landmarks.append(("Bookshelf", -0.09, 3.68))
landmarks.append(("Barrier", -2.42, 1.4))
landmarks.append(("Cylinder", -2.34, -0.41))
landmarks.append(("Cube", 0.15, 1.65))

def distance(x1, y1, x2, y2):
    xd = x1 - x2
    yd = y1 - y2
    return math.sqrt(xd * xd + yd * yd)

def callback(msg):
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    closest_name = None
    closest_distance = None
    for l_name, l_x, l_y in landmarks:
        dist = distance(x, y, l_x, l_y)
        if closest_distance is None or dist < closest_distance:
            closest_distance = dist
            closest_name = l_name
    rospy.loginfo('closest object: {}'.format(closest_name))


def main():
    rospy.init_node('location_monitor')

    rospy.Subscriber("/odom", Odometry, callback)

    rospy.spin()


if __name__ == '__main__':
    main()

