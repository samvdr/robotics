#!/usr/bin/env python

import rospy
import numpy
import cv2
from nav_msgs.msg import Odometry
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from geometry_msgs.msg import Twist
from math import radians


class KleurDetectie:
    def __init__(self):
        # make move variables
        self.move_cmd = None
        self.turnl_cmd = None
        self.turnr_cmd = None
        # initialize them
        self.init_moves()

        # the coordinates of the turtlebot
        self.x_mid_gate = None
        self.y_mid_gate = None

        self.stopped = False

        self.bridge = CvBridge()

        # Connect image topic
        img_topic = "/camera/rgb/image_raw"
        self.image_sub = rospy.Subscriber(img_topic, Image, self.callback_img)

        # connects to the cmd_vel topic wich controls the turtlebot
        self.cmd_vel_pub = rospy.Publisher('cmd_vel_mux/input/teleop', Twist, queue_size=10)

        self.twist = Twist()
        # Allow up to one second to connection
        rospy.sleep(1)

    def callback_img(self, data):
        cv_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='bgr8')
        hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

        # zoek geel
        lower_geel = numpy.array([10, 10, 10])
        upper_geel = numpy.array([255, 255, 250])
        mask_geel = cv2.inRange(hsv, lower_geel, upper_geel)

        # zoek rood
        lower_rood = numpy.array([0, 50, 50])
        upper_rood = numpy.array([10, 255, 255])
        mask_rood = cv2.inRange(hsv, lower_rood, upper_rood)

        mg = cv2.moments(mask_geel)
        mr = cv2.moments(mask_rood)

        # hij ziet geel en rood
        if mg['m00'] > 0 and mr['m00'] > 0:
            cgx = int(mg['m10'] / mg['m00'])
            cgy = int(mg['m01'] / mg['m00'])
            crx = int(mr['m10'] / mr['m00'])
            cry = int(mr['m01'] / mr['m00'])
            cv2.circle(cv_image, (cgx, cgy), 20, (0, 0, 255), -1)
            cv2.circle(cv_image, (crx, cry), 20, (255, 0, 0), -1)
            # berekent middelpunt
            self.x_mid_gate = (cgx + crx) / 2
            self.y_mid_gate = (cgy + cry) / 2
            # methode die zorgt dat hij blijft rijden tot de turtlebot tussen de balken staat
            self.move_till_middle()

        # hij ziet enkel rood
        if mr['m00'] > 0:
            self.cmd_vel_pub.publish(self.turnl_cmd)

        # hij ziet enkel geel
        if mg['m00'] > 0:
            self.cmd_vel_pub.publish(self.turnr_cmd)

        # hij ziet geen kleuren
        if mg['m00'] < 0 and mr['m00'] < 0:
            self.cmd_vel_pub.publish(self.turnr_cmd)

        cv2.imshow("window", cv_image)
        cv2.waitKey(3)

    def move_till_middle(self):
        while True:
            # komt in een kunstmatige do-while loop
            # weet niet of dit werkt!
            rospy.Subscriber("/odom", Odometry, self.callback_odom)

            # stopt als stopped = true
            if self.stopped:
                break

    def callback_odom(self, msg):
        self.cmd_vel_pub.publish(self.move_cmd)

        # pakt de x_turtlebot en y_turtlebot waarde van de turtlebot
        x_turtlebot = msg.pose.pose.position.x
        y_turtlebot = msg.pose.pose.position.y

        self.stop(x_turtlebot, y_turtlebot, 0.1)

    def stop(self, x_turtlebot, y_turtlebot, threshold):
        xd = None
        yd = None

        # neemt de verschillen
        if x_turtlebot > self.x_mid_gate:
            xd = x_turtlebot - self.x_mid_gate
        else:
            xd = self.x_mid_gate - x_turtlebot

        if y_turtlebot > self.y_mid_gate:
            yd = y_turtlebot - self.y_mid_gate
        else:
            yd = self.y_mid_gate - y_turtlebot

        # threshold is een mogelijke afwijking
        if xd <= threshold and yd <= threshold:
            self.cmd_stop()

    def cmd_stop(self):
        self.move_cmd.linear.x = 0
        self.cmd_vel_pub.publish(self.move_cmd)
        self.stopped = True
        self.move_cmd.linear.x = 0.2

    def init_moves(self):
        # vooruit rijden
        self.move_cmd = Twist()
        self.move_cmd.linear.x = 0.2

        # Naar rechts draaien
        self.turnr_cmd = Twist()
        self.turnr_cmd.linear.x = 0
        self.turnr_cmd.angular.z = radians(-45)

        # Naar links draaien
        self.turnl_cmd = Twist()
        self.turnl_cmd.linear.x = 0
        self.turnl_cmd.angular.z = radians(45)


def main():
    rospy.init_node('KleurDetectie_2.1')
    KleurDetectie()
    rospy.spin()


if __name__ == '__main__':
    main()
