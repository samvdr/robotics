#!/usr/bin/env python

import rospy
import numpy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from geometry_msgs.msg import Twist
import math
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import PointCloud2


class HerkenVorm:
    def __init__(self):
        rospy.init_node('vormHerkenning', anonymous=False)
        self.bridge = CvBridge()

        self.mg = None
        self.mr = None
        self.cv_image = None
        self.height = None
        self.middle_x = None
        self.float_data = None
        self.cloud_point = PointCloud2()
        self.depth = None
        self.dist_to_gate = None

        # Connect image topic
        img_topic = "/camera/rgb/image_raw"
        self.image_sub = rospy.Subscriber(img_topic, Image, self.callback_image)

        # self.image_sub = rospy.Subscriber(img_topic, Image, self.callback_odom)
        self.cmd_vel_pub = rospy.Publisher('cmd_vel_mux/input/teleop', Twist, queue_size=10)
        self.twist = Twist()
        # Allow up to one second to connection
        rospy.sleep(1)

    def callback_kinect(self, data):
        self.height = int(data.height / 2)
        self.middle_x = int(data.width / 2)
        self.cloud_point = data

        self.read_depth(self.middle_x, self.height, data)

    def callback_image(self, data):
        self.cv_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='bgr8')
        hsv = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2HSV)

        # toon wat de turtlebot ziet
        cv2.imshow("window", self.cv_image)
        cv2.waitKey(3)

        # zoek geel
        lower_geel = numpy.array([10, 10, 10])
        upper_geel = numpy.array([255, 255, 250])
        mask_geel = cv2.inRange(hsv, lower_geel, upper_geel)

        # zoek rood
        lower_rood = numpy.array([0, 50, 50])
        upper_rood = numpy.array([10, 255, 255])
        mask_rood = cv2.inRange(hsv, lower_rood, upper_rood)

        self.mg = cv2.moments(mask_geel)
        self.mr = cv2.moments(mask_rood)

    def ride(self):
        # Naar rechts draaien
        turnr_cmd = Twist()
        turnr_cmd.linear.x = 0
        turnr_cmd.angular.z = math.radians(-10)

        # Naar links draaien
        turnl_cmd = Twist()
        turnl_cmd.linear.x = 0
        turnl_cmd.angular.z = math.radians(10)

        # wat doet dit?
        # # Naar links draaien
        # turns_cmd = Twist()
        # turns_cmd.linear.x = 0
        # turns_cmd.angular.z = math.radians(0)

        # blijft draaien tot hij rood en geel ziet
        while self.mg['m00'] == 0 or self.mr['m00'] == 0:
            if self.mg['m00'] == 0:
                # rospy.loginfo('zoek geel')
                self.cmd_vel_pub.publish(turnl_cmd)

            if self.mr['m00'] == 0:
                # rospy.loginfo('zoek rood')
                self.cmd_vel_pub.publish(turnr_cmd)

            #self.cmd_vel_pub.publish(turnr_cmd)

        # self.cmd_vel_pub.publish(turns_cmd)
        # rospy.loginfo('ik zie rood en geel')
        self.center()

    def center(self):
        # stopt met draaien
        turns_cmd = Twist()
        turns_cmd.linear.x = 0
        turns_cmd.angular.z = math.radians(0)

        # correctie naar rechts
        cr_cmd = Twist()
        cr_cmd.linear.x = 0
        cr_cmd.angular.z = math.radians(-1)

        # correctie naar links
        cl_cmd = Twist()
        cl_cmd.linear.x = 0
        cl_cmd.angular.z = math.radians(1)

        centered = False

        # middelpunt camera
        height, width = self.cv_image.shape[:2]
        mcamera = width / 2

        self.get_distance("geel", mcamera, cl_cmd, cr_cmd, turns_cmd)
        gdist = self.float_data
        self.get_distance("rood", mcamera, cl_cmd, cr_cmd, turns_cmd)

        while not centered:
            # hij ziet rood en geel
            cgx = int(self.mg['m10'] / self.mg['m00'])
            crx = int(self.mr['m10'] / self.mr['m00'])

            x_m_gate = (cgx + crx) / 2

            if x_m_gate < mcamera:
                self.cmd_vel_pub.publish(cl_cmd)
            if x_m_gate > mcamera:
                self.cmd_vel_pub.publish(cr_cmd)
            else:
                centered = True

        # self.cmd_vel_pub.publish(turns_cmd)
        # rospy.loginfo('ik sta recht voor het midden en begin te rijden')

        self.ride_to_middle()

    def get_distance(self, color, mcamera, cl, cr, cs):
        dist = None
        look_at_pole = False

        if color == "geel":

            while not look_at_pole:
                # hij ziet geel
                cgx = int(self.mg['m10'] / self.mg['m00'])

                if cgx < mcamera:
                    self.cmd_vel_pub.publish(cl)
                if cgx > mcamera:
                    self.cmd_vel_pub.publish(cr)
                else:
                    tmp = rospy.Subscriber("/camera/depth/points", PointCloud2, self.callback_kinect)
                    look_at_pole = True
                    tmp.unregister()

            # if (self.height >= self.cloud_point.height) or (self.middle_x >= self.cloud_point.width):
            #     return -1
            # data_out = pc2.read_points(self.cloud_point, field_names="z", skip_nans=False, uvs=[[self.middle_x, self.height]])
            # dist = self.read_depth(self.middle_x, self.height, self.cloud_point)
            print("afstand tot geel is {}".format(self.float_data))

        # else:
        #     crx = int(self.mr['m10'] / self.mr['m00'])
        #     while not crx == mcamera:
        #         self.cmd_vel_pub.publish(cr)
        #
        #     self.cmd_vel_pub.publish(cs)
        #
        #     dist = self.depth
        #
        #     print("afstand tot rood is {}".format(dist))

    def read_depth(self, width, height, data):
        if (height >= data.height) or (width >= data.width):
            return -1
        data_out = pc2.read_points(data, field_names="z", skip_nans=False, uvs=[[width, height]])
        self.float_data = float(next(data_out)[0])

    def ride_to_middle(self):
        #Komt hier and of or?
        while self.mg['m00'] > 0 and self.mr['m00'] > 0:
            # vooruit rijden
            move_cmd = Twist()
            move_cmd.linear.x = 0.2

            self.cmd_vel_pub.publish(move_cmd)

        #herbereken
        self.ride()


if __name__ == '__main__':
    HerkenVorm = HerkenVorm()

    HerkenVorm.ride()

    rospy.spin()
